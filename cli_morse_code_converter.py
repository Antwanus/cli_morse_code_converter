INTERNATIONAL_MORSE_DICT = {
    ' ': '\t/\t',
    'a': '*-',
    'b': '-***',
    'c': '-*-*',
    'd': '-**',
    'e': '*',
    'f': '**-*',
    'g': '--*',
    'h': '****',
    'i': '**',
    'j': '*---',
    'k': '-*-',
    'l': '*-**',
    'm': '--',
    'n': '-*',
    'o': '---',
    'p': '*--*',
    'q': '--*-',
    'r': '*-*',
    's': '***',
    't': '-',
    'u': '**-',
    'v': '***-',
    'w': '*--',
    'x': '-**-',
    'y': '-*--',
    'z': '--**',
    '0': '-----',
    '1': '*----',
    '2': '**---',
    '3': '***--',
    '4': '****-',
    '5': '*****',
    '6': '-****',
    '7': '--***',
    '8': '---**',
    '9': '----*',
    ', ': '--..--',
    '.': '.-.-.-',
    '?': '..--..',
    '/': '-..-.',
    '-': '-....-',
    '(': '-.--.',
    ')': '-.--.-',
}

user_input = input('Please provide a word, number or phrase to convert to morse:\n').lower()

result = []
for word in user_input.split(' '):
    for character in word:
        result += INTERNATIONAL_MORSE_DICT[character]
        if character is word[-1]:
            # => character is the last in word
            if word is not user_input.split(' ')[-1]:
                # => word is not the last of input ==> add space between words
                result += INTERNATIONAL_MORSE_DICT[' ']
        else:
            # => character is not last in word ==> add space between letters
            result += ' '

print(''.join(result))
